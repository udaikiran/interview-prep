# Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
# You may assume that each input would have exactly one solution, and you may not use the same element twice.
# You can return the answer in any order.

from typing import List

def twoSum(nums: List[int], target: int) -> List[int]:

    for num1 in range(len(nums)):
        for num2 in range(num1+1, len(nums)):
            two_sum = nums[num1] + nums[num2]
            print(two_sum, num1, num2, nums[num1], nums[num2])
            if two_sum == target:
                return [num1, num2]

if __name__ == '__main__':
    print(twoSum(nums=[2, 7, 11, 15], target=9))
    print(twoSum(nums = [3,2,4], target = 6))