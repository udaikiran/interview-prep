# Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
# You may assume that each input would have exactly one solution, and you may not use the same element twice.
# You can return the answer in any order.

from typing import List, Dict


def twoSum(nums: List[int], target: int) -> List[int]:
    hash_map: Dict[int, int] = dict()

    for num in range(len(nums)):
        if nums[num] > target:
            continue

        if nums[num] < 0:
            continue

        if hash_map.get(target - nums[num]) is not None:
            return [hash_map.get(target - nums[num]), num]
        else:
            hash_map[nums[num]] = num
    return []


if __name__ == '__main__':
    print(twoSum(nums=[2, 7, 11, 15], target=9))
    print(twoSum(nums=[3, 2, 4], target=6))
    print(twoSum(nums=[-3, 4, 3, 90], target=0))
