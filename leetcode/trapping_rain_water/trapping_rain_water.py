# leet code problem 42.

from typing import List


def trap(height: List[int]) -> int:
    total_water = 0

    if height:
        max_left = height[0]
        max_right = height[len(height)-1]

    p_left = 0
    p_right = len(height)-1

    while(p_left <= p_right):

        if height[p_left] <= height[p_right]:

            if max_left <= height[p_left]:
                max_left = height[p_left]
            else:
                total_water += (max_left - height[p_left])

            p_left += 1
        else:
            if max_right <= height[p_right]:
                max_right = height[p_right]
            else:
                total_water += (max_right - height[p_right])

            p_right -= 1

    return total_water


if __name__ == '__main__':
    print(trap(height=[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))
    # assert trap(height=[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]) == 6
    #assert trap(height=[4,2,0,3,2,5]) == 6
