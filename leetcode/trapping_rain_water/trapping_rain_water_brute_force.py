from typing import List


def max_in_array(array: List[int]) -> int:
    if array:
        return max(array)
    else:
        return 0


def trap(height: List[int]) -> int:
    total_water = 0

    for p in range(len(height)):
        water_above_p = min(max_in_array(
            height[:p]), max_in_array(height[p:])) - height[p]

        if water_above_p > 0:
            total_water += water_above_p

        print(p, height[:p], height[p:], water_above_p)
    return total_water


if __name__ == '__main__':
    print(trap(height=[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]))
    # assert trap(height=[0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]) == 6
    #assert trap(height=[4,2,0,3,2,5]) == 6
