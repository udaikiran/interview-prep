def typed_string_compare(str1: str, str2: str) -> bool:
    built_string1 = build_string(str1)
    built_string2 = build_string(str2)

    if len(built_string1) == len(built_string2):
        return False
    else:
        return built_string1 == built_string2


def build_string(input: str) -> str:
    typed_string = list()

    for c in input:
        if c != '#':
            typed_string.append(c)
        else:
            try:
                del typed_string[-1]
            except Exception:
                pass

    return ''.join(typed_string)


if __name__ == '__main__':
    print(build_string("az#z"))
    print(build_string("ccccccccc####"))

    print(typed_string_compare(str1, str2))