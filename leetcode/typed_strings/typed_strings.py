# def typed_string_compare(str1, str2) -> bool:
#     for p in range(max(len(str1), len(str2)), 0):
#         print(p)
#     return True

def typed_generator(typed_str: str):
    count = 0
    for c in reversed(typed_str):

        if count:
            if c == '#':
                count += 1
            else:
                count -= 1

            continue

        if c != '#':
            count = 0
            yield c
        else:
            count += 1
            continue


if __name__ == '__main__':
    # print(typed_string_compare("az#z", "ap#z"))
    print(list(typed_generator("ab##")))
    print(list(typed_generator("ad#c")))

