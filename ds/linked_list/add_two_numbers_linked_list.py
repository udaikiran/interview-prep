# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def addTwoNumbers(self, l1: Optional[ListNode], l2: Optional[ListNode]) -> Optional[ListNode]:
        c = 0
        ret = None
        result = None

        while l1 or l2:
            if l1:
                a = l1.val
            else:
                a = 0

            if l2:
                b = l2.val
            else:
                b = 0

            r = a + b + c
            if r >= 10:
                c = 1
                r = r % 10
            else:
                c = 0
            if not result:
                result = ListNode(val=r, next=None)
                ret = result
            else:
                result.next = ListNode(val=r, next=None)
                result = result.next

            if l1:
                l1 = l1.next

            if l2:
                l2 = l2.next

        if c:
            result.next = ListNode(val=c, next=None)

        return ret

    def addTwoNumbers2(self, l1, l2):
        carry = 0
        res = n = ListNode(0)
        while l1 or l2 or carry:
            if l1:
                carry += l1.val
                l1 = l1.next
            if l2:
                carry += l2.val
                l2 = l2.next
            carry, val = divmod(carry, 10)
            n.next = n = ListNode(val)
        return res.next
