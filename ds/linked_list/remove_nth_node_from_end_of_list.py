# Given the head of a linked list, remove the nth node from the end of the list and return its head.
from typing import Optional


# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    # To do that, we can simply stagger our two pointers by n nodes by giving the first pointer (fast)
    # a head start before starting the second pointer (slow). Doing this will cause slow to reach
    # the n'th node from the end at the same time that fast reaches the end.
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        fast, slow = head, head
        for _ in range(n):
            fast = fast.next
        if not fast:
            return head.next

        while fast.next:
            fast, slow = fast.next, slow.next

        slow.next = slow.next.next
        return head
