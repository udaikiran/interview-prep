"""
Given the head of a singly linked list,
return true if it is a palindrome or false otherwise.
Input: head = [1,2,2,1]
Output: true

Input: head = [1,2]
Output: false
"""


# Definition for singly-linked list.
from typing import Optional


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next

class Solution:
    def isPalindrome_O_n(self, head):
        vals = []

        while head:
            vals += head.val,
            head = head.next
        return vals == vals[::-1]

    def isPalindrome_rev_list(self, head: Optional[ListNode]) -> bool:
        # Phase 1: Reverse the first half while finding the middle.
        # Phase 2: Compare the reversed first half with the second half.
        rev = None
        slow = fast = head
        while fast and fast.next:
            fast = fast.next.next
            rev, rev.next, slow = slow, rev, slow.next
        if fast:
            slow = slow.next
        while rev and rev.val == slow.val:
            slow = slow.next
            rev = rev.next
        return not rev

    def isPalindrome_with_list_restore(self, head):
        rev = None
        fast = head
        while fast and fast.next:
            fast = fast.next.next
            rev, rev.next, head = head, rev, head.next
        tail = head.next if fast else head
        isPali = True
        while rev:
            isPali = isPali and rev.val == tail.val
            head, head.next, rev = rev, head, rev.next
            tail = tail.next
        return isPali
