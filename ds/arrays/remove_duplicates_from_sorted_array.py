#!/usr/bin/env python3
from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        nda = set()
        
        idx = 0 
        for num in range(len(nums)):
            if nums[num] in nda:
                continue
            else:
                nums[idx] = nums[num]
                idx += 1
                nda.add(nums[num])

        return idx
