"""
Given a binary array nums, return the maximum number of consecutive 1's in the array.

level : Easy

"""
from typing import List


class Solution:
    def findMaxConsecutiveOnes(self, nums: List[int]) -> int:
        max_consec_ones_count = 0
        local_consec_ones_count = 0
        
        for n in nums:
            if n:
                local_consec_ones_count += 1
                max_consec_ones_count = max(max_consec_ones_count, local_consec_ones_count)
            else:
                local_consec_ones_count = 0

        return max_consec_ones_count
