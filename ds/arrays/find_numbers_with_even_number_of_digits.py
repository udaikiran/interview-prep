"""
Given an array nums of integers, return how many of them contain an even number of digits.

"""
from typing import List


class Solution:
    def findNumbers(self, nums: List[int]) -> int:
        num_count = 0
        
        def number_of_digits(n):
            count = 0
            while(n != 0):
                n //= 10
                count = count + 1

            return count

        for num in nums:
            if not (number_of_digits(num) % 2):
                num_count += 1

        return num_count
